<?php
include_once '../apporioconfig/start_up.php';
if(!isset($_SESSION['ADMIN']))
{
    $db->redirect("index.php");
}
include('common.php');
$query="select * from city";
$result = $db->query($query);
$list=$result->rows;
?>

<form method="post" name="frm">
    <div class="wraper container-fluid">
        <div class="page-title">
            <h3 class="title">View City</h3>
        </div>
        <div class="row">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12 mobtbl">
                                <table id="datatable" class="table table-striped table-bordered table-responsive">
                                    <thead>
                                    <tr>
                                        <th>City Name</th>
                                        <th>Currency</th>
                                        <th>Distance</th>
                                        <th width="10%">Status</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach($list as $city){ ?>
                                        <tr>
                                            <td>
                                                <?php
                                                $city_name=$city['city_name'];
                                                echo $city_name;
                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                $currency_name=$city['currency'];
                                                echo $currency_name;
                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                $distance=$city['distance'];
                                                echo $distance;
                                                ?>
                                            </td>
                                            <?php
                                            if($city['city_admin_status']==1) {
                                                ?>
                                                <td class="text-center">

                                                    <label class="label label-success" > Active</label>
                                                </td>
                                                <?php
                                            } else {
                                                ?>
                                                <td class="text-center">

                                                    <label class="label label-default" > Deactive</label>
                                                </td>
                                            <?php } ?>
                                            <td>
                                                <div class="row action_row" style="width:95px;">
                                                    <span data-target="#<?php echo $city['city_id'];?>" data-toggle="modal"><a data-original-title="Edit"  data-toggle="tooltip" data-placement="top" class="btn menu-icon btn_edit"> <i class="fa fa-pencil"></i> </a></span>

                                                    <?php if($city['city_admin_status']==1){ ?>
                                                        <a data-original-title="Active" class="btn menu-icon btn_eye" href="home.php?pages=view-city&status=2&id=<?php echo $city['city_id']?>"> <i class="fa fa-eye"></i> </a>
                                                    <?php } else { ?>
                                                        <a data-original-title="Inactive" data-toggle="tooltip" data-placement="top" class="btn menu-icon btn_eye_dis" href="home.php?pages=view-city&status=1&id=<?php echo $city['city_id']?>"> <i class="fa fa-eye-slash"></i> </a>
                                                    <?php } ?>
                                                    <!--<span data-target="#delete<?php /*echo $city['city_id'];*/?>" data-toggle="modal"><a data-original-title="Delete"  data-toggle="tooltip" data-placement="top" class="btn menu-icon btn_delete"> <i class="fa fa-trash"></i> </a></span-->
                                                </div>
                                            </td>
                                        </tr>
                                    <?php }?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

        </div>

        <!-- End row -->

    </div>
</form>
    </section>
<!-- Main Content Ends -->

</body></html>