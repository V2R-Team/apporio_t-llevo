<?php
include_once '../apporioconfig/start_up.php';
if(!isset($_SESSION['ADMIN']['ID']))
{
    $db->redirect("index.php");
}
include('common.php');
$query = "select * from currency";
$result = $db->query($query);
$list = $result->rows;

if(isset($_POST['save']))
{

    $name = $_POST['name'];
    $code = $_POST['code'];
    $query2="INSERT INTO currency (name,code,symbol) VALUES ('$name','$code','$code')";
    $db->query($query2);
    $msg = "Currency Details Saved Successfully";
    echo '<script type="text/javascript">alert("'.$msg.'")</script>';
    $db->redirect("home.php?pages=add-currency");
}

?>

<div class="wraper container-fluid">
    <div class="page-title">
        <h3 class="title">Add Currency</h3>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">

                <div class="panel-body">
                    <div class="form">
                        <form class="cmxform form-horizontal tasi-form"  method="post"  onSubmit="return validatelogin()">
                            <div class="form-group ">
                                <label class="control-label col-lg-2">Currency Name*</label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control"  placeholder="Currency Name" name="name" id="name" >
                                </div>
                            </div>

                            <div class="form-group ">
                                <label class="control-label col-lg-2">Currency Code Only In Html*</label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control"  placeholder="Currency Code Only In Html" name="code" id="code">
                                    <a target="_blank" href="https://www.toptal.com/designers/htmlarrows/currency/"><h5>For Html Code Plz Refer https://www.toptal.com/designers/htmlarrows/currency/ </h5></a>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <input type="submit" class=" btn btn-info col-md-4 col-sm-6 col-xs-12 black-background white" id="save" name="save" value="Save" >
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- .form -->

                </div>
                <!-- panel-body -->
            </div>
            <!-- panel -->
        </div>
        <!-- col -->

    </div>
    <!-- End row -->

</div>

<!-- Page Content Ends -->
<div class="wraper container-fluid">
    <div class="page-title">
        <h3 class="title">Currency</h3>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">

                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 mobtbl">
                            <table id="datatable" class="table table-striped table-bordered table-responsive">
                                <thead>
                                <tr>
                                    <th>Sr.No</th>
                                    <th>Currency Name</th>
                                    <th>Currency Symbol</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $i = 1;
                                foreach($list as $currency){?>
                                    <tr>
                                        <td><?= $i; ?></td>
                                        <td>
                                            <?php
                                            $name = $currency['name'];
                                            echo $name;
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            $code = $currency['code'];
                                            echo $code;
                                            ?>
                                        </td>
                                    </tr>
                                    <?php
                                    $i++;
                                }?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<!-- ================== -->

</section>
<!-- Main Content Ends -->

</body>
</html>
