<?php
include_once '../apporioconfig/start_up.php';
if(!isset($_SESSION['ADMIN']['ID']))
{
    $db->redirect("index.php");
}
include('common.php');

if(isset($_POST['save'])) 
{
        $rental_category = $_POST['rental_category'];
        $query="select * from rental_category WHERE rental_category='$rental_category'";
	$result = $db->query($query);
        $ex_rows = $result->num_rows;
        if($ex_rows == 0)
        {
                $query2="INSERT INTO rental_category(rental_category_distance_unit,rental_category,rental_category_hours,rental_category_kilometer,rental_category_description) VALUES ('".$_POST['rental_category_distance_unit']."','".$_POST['rental_category']."','".$_POST['rental_category_hours']."','".$_POST['rental_category_kilometer']."','".$_POST['description']."')";
		$db->query($query2);
		$car_type_id = $db->getLastId();
                $msg = "Category Added Successfully";
                echo '<script type="text/javascript">alert("'.$msg.'")</script>';
               $db->redirect("home.php?pages=add-rental-category");
          
        }else{
                $msg = "Category Already Added";
                echo '<script type="text/javascript">alert("'.$msg.'")</script>';
               $db->redirect("home.php?pages=add-rental-category");
        }
}

?>
<link rel="stylesheet" type="text/css" href="taxi/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
 <link href="taxi/summernote/summernote.css" rel="stylesheet" />

<script>
    function validatelogin() {
        var rental_category = document.getElementById('rental_category').value;
        var rental_category_hours = document.getElementById('rental_category_hours').value;
        var rental_category_kilometer = document.getElementById('rental_category_kilometer').value;
		var description = document.getElementById('description').value;
		var rental_category_distance_unit = document.getElementById('rental_category_distance_unit').value;
        if(rental_category == "")
        {
            alert("Enter Rental Package Name");
            return false;
        }
        if(rental_category_hours == "")
        {
            alert("Enter Travel Hours");
            return false;
        }
        if(rental_category_kilometer == "")
        {
            alert("Enter Travel Kilometre");
            return false;
        }
		if(description == "")
        {
            alert("Enter Package Description");
            return false;
        }
        if(rental_category_distance_unit == "")
        {
            alert("Enter Distance Unit");
            return false;
        }
       
    }
    function isNumber(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }
</script>

  <div class="wraper container-fluid">
    <div class="page-title">
      <h3 class="title">Add Rental Package</h3>
        
            <span class="tp_rht">
            <a href="home.php?pages=rental-category" data-toggle="tooltip" title="" class="btn btn-default" data-original-title="Back"><i class="fa fa-reply"></i></a>
      </span>
       
    </div>
    <div class="row">
      <div class="col-sm-12">
        <div class="panel panel-default">
          
          <div class="panel-body">
            <div class=" form" >
              <form class="cmxform form-horizontal tasi-form"  method="post" enctype="multipart/form-data" onSubmit="return validatelogin()">


                  <div class="form-group ">
                      <label for="lastname" class="control-label col-lg-2">Rental Package Name*</label>
                      <div class="col-lg-6">
                          <input type="text" class="form-control" placeholder="Rental Package Name" name="rental_category" id="rental_category"/>
                      </div>
                  </div>

                <div class="form-group ">
                  <label for="lastname" class="control-label col-lg-2">Travel Hours*</label>
                  <div class="col-lg-6">
                    <input type="text" class="form-control" placeholder="Travel Hours" name="rental_category_hours" onkeypress="return isNumber(event)" id="rental_category_hours"/>
                  </div>
                </div>

                  <div class="form-group ">
                      <label for="lastname" class="control-label col-lg-2">Travel Distance*</label>
                      <div class="col-lg-6">
                          <input type="text" class="form-control" placeholder="Travel Distance" name="rental_category_kilometer" onkeypress="return isNumber(event)" id="rental_category_kilometer"/>
                      </div>
                  </div>

                  <div class="form-group ">
                      <label class="control-label col-lg-2">Distance Unit*</label>
                      <div class="col-lg-6">
                          <select class="form-control" name="rental_category_distance_unit" id="rental_category_distance_unit" >
                              <option value="">--Please Select Distance Unit--</option>
                              <option value="Miles">Miles</option>
                              <option value="Km">Km</option>
                          </select>
                      </div>
                  </div>

                  <div class="form-group">
                   <label class="col-md-2 control-label">Package Description*</label>
                   <div class="col-md-6">
                      <textarea class="summernote" name="description" id="description" placeholder="Description"></textarea>
                   </div>
                </div>
                
                <div class="form-group">
                  <div class="col-lg-offset-2 col-lg-10">
                    <input type="submit" class=" btn btn-info col-md-4 col-sm-6 col-xs-12 black-background white" id="save" name="save" value="Save" >
                  </div>
                </div>
              </form>
            </div>
            <!-- .form --> 
            
          </div>
          <!-- panel-body --> 
        </div>
        <!-- panel --> 
      </div>
      <!-- col --> 
      
    </div>
    <!-- End row --> 
    
  </div>
  
  <!-- Page Content Ends --> 
  <!-- ================== --> 
  
</section>
<!-- Main Content Ends -->
<script type="text/javascript" src="taxi/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
        <script type="text/javascript" src="taxi/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>

        
        <script src="taxi/summernote/summernote.min.js"></script>
		<script src="js/jquery.app.js"></script>
		<script>

            jQuery(document).ready(function(){
                $('.wysihtml5').wysihtml5();

                $('.summernote').summernote({
                    height: 200,                 // set editor height

                    minHeight: null,             // set minimum height of editor
                    maxHeight: null,             // set maximum height of editor

                    focus: true                 // set focus to editable area after initializing summernote
                });

            });
        </script>
</body>
</html>
