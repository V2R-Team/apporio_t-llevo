<?php
include_once '../apporioconfig/start_up.php';
if(!isset($_SESSION['ADMIN']['ID']))
{
    $db->redirect("index.php");
}
include('common.php');
$query = "select * from admin_panel_settings WHERE admin_panel_setting_id=1";
$result = $db->query($query);
$admin_settings = $result->row;
$admin_panel_firebase_id = $admin_settings['admin_panel_firebase_id'];
if(isset($_POST['delete']))
{
    $url = 'https://'.$admin_panel_firebase_id.'.firebaseio.com/Drivers_A/.json';
    $ch = curl_init();
    curl_setopt($ch,CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
    $response = curl_exec($ch);
    $url = 'https://'.$admin_panel_firebase_id.'.firebaseio.com/RideTable/.json';
    $ch = curl_init();
    curl_setopt($ch,CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
    $response = curl_exec($ch);
    $url = 'https://'.$admin_panel_firebase_id.'.firebaseio.com/Activeride/.json';
    $ch = curl_init();
    curl_setopt($ch,CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
    $response = curl_exec($ch);
    $url = 'https://'.$admin_panel_firebase_id.'.firebaseio.com/geofire/.json';
    $ch = curl_init();
    curl_setopt($ch,CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
    $response = curl_exec($ch);
    $msg = "Driver Clear On Firebase";
    echo '<script type="text/javascript">alert("'.$msg.'")</script>';
    $db->redirect("home.php?pages=developer-tools");
}

if(isset($_POST['delete1']))
{
    $url = 'https://'.$admin_panel_firebase_id.'.firebaseio.com/RideTable/.json';
    $ch = curl_init();
    curl_setopt($ch,CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
    $response = curl_exec($ch);
    $url = 'https://'.$admin_panel_firebase_id.'.firebaseio.com/Activeride/.json';
    $ch = curl_init();
    curl_setopt($ch,CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
    $response = curl_exec($ch);
    $url = 'https://'.$admin_panel_firebase_id.'.firebaseio.com/USER/.json';
    $ch = curl_init();
    curl_setopt($ch,CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
    $response = curl_exec($ch);
    $msg = "User Clear On Firebase";
    echo '<script type="text/javascript">alert("'.$msg.'")</script>';
    $db->redirect("home.php?pages=developer-tools");
}

if(isset($_POST['delete2']))
{
    $url = 'https://'.$admin_panel_firebase_id.'.firebaseio.com/RideTable/.json';
    $ch = curl_init();
    curl_setopt($ch,CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
    $response = curl_exec($ch);
    $url = 'https://'.$admin_panel_firebase_id.'.firebaseio.com/Activeride/.json';
    $ch = curl_init();
    curl_setopt($ch,CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
    $response = curl_exec($ch);
    $msg = "Rides Clear On Firebase";
    echo '<script type="text/javascript">alert("'.$msg.'")</script>';
    $db->redirect("home.php?pages=developer-tools");
}

?>
<div class="wraper container-fluid">
    <div class="page-title">
        <h3 class="title">Developer Tools</h3>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">

                <div class="panel-body">
                    <div class="form">

                        <div class="form-group col-lg-12">
                            <label for="lastname" class="control-label col-lg-2">Delete Driver</label>
                            <div class="col-lg-3">
                                <button type="button" id="get_driver" data-target="#Driver" data-toggle="modal" class="btn btn-danger col-md-12">Delete On Firebase</button>
                            </div>
                         
                        </div>

                        <div class="form-group col-lg-12">
                            <label for="lastname" class="control-label col-lg-2">Delete User</label>
                            <div class="col-lg-3">
                                <button type="button" id="get_user" data-toggle="modal" data-target="#User" class="btn btn-danger col-md-12">Delete On Firebase</button>
                            </div>
                            
                        </div>

                        <div class="form-group col-lg-12">
                            <label for="lastname" class="control-label col-lg-2">Delete Rides</label>
                            <div class="col-lg-3">
                                <button type="button" id="get_ride" data-toggle="modal" data-target="#rides" class="btn btn-danger col-md-12">Delete On Firebase</button>
                            </div>
                           
                        </div>

                    </div>
                    <!-- .form -->

                </div>
                <!-- panel-body -->
            </div>
            <!-- panel -->
        </div>
        <!-- col -->

    </div>
    <!-- End row -->

</div>

<div class="modal fade" id="Driver" role="dialog">
    <div class="modal-dialog">
        <form method="post">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title fdetailsheading">Delete Drivers </h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <h3>Do You Really Want To Delete The Driver?</h3></div>
                    <div class="modal-footer">
                        <button type="submit" name="delete" value="driver" class="btn btn-danger">Delete</button>
                        <button type="button" class="btn btn-info" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>



<div class="modal fade" id="User" role="dialog">
    <div class="modal-dialog">
        <form method="post">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title fdetailsheading">Delete Users </h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <h3>Do You Really Want To Delete The User?</h3></div>
                    <div class="modal-footer">
                        <button type="submit" name="delete1" value="driver" class="btn btn-danger">Delete</button>
                        <button type="button" class="btn btn-info" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<div class="modal fade" id="rides" role="dialog">
    <div class="modal-dialog">
        <form method="post">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title fdetailsheading">Delete Rides </h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <h3>Do You Really Want To Delete The Rides?</h3></div>
                    <div class="modal-footer">
                        <button type="submit" name="delete2" value="driver" class="btn btn-danger">Delete</button>
                        <button type="button" class="btn btn-info" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
</section>
<!-- Main Content Ends -->

</body>
</html>
