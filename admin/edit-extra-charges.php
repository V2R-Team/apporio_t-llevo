<?php
session_start();
include_once '../apporioconfig/start_up.php';
if($_SESSION['ADMIN']['ID'] == "")
{
    $db->redirect("home.php?pages=index");
}
include('common.php');
$extra_charges_id = $_GET['id'];
$query="select * from extra_charges WHERE extra_charges_id='$extra_charges_id'";
$result = $db->query($query);
$list=$result->row;
if ($_POST['Update'])
{
    $extra_charges_type = $_POST['extra_charges_type'];
    if ($extra_charges_type == 1)
    {
        $query2="UPDATE extra_charges SET slot_one_starttime='".$_POST['slot_one_starttime']."',slot_one_endtime='".$_POST['slot_one_endtime']."',slot_two_starttime='".$_POST['slot_two_starttime']."',slot_two_endtime='".$_POST['slot_two_endtime']."',payment_type='".$_POST['payment_type']."',slot_price='".$_POST['slot_price']."' where extra_charges_id='".$_POST['extra_charges_id']."'";
        $db->query($query2);
    }else{
        $query2="UPDATE extra_charges SET slot_one_starttime='".$_POST['slot_one_starttime']."',slot_one_endtime='".$_POST['slot_one_endtime']."',payment_type='".$_POST['payment_type']."',slot_price='".$_POST['slot_price']."' where extra_charges_id='".$_POST['extra_charges_id']."'";
        $db->query($query2);
    }
    echo '<script type="text/javascript">alert("Extra Charges Updated Successfully")</script>';
    $db->redirect("home.php?pages=view-rate-card");
}
?>
<div class="wraper container-fluid">
    <div class="page-title">
        <h3 class="title">Edit Extra charges</h3>
    </div>

    <div class="row">
        <div class="col-sm-12">

            <div class="panel panel-default">
                <div class="form" >
                    <form class="cmxform form-horizontal tasi-form" name="peak" onSubmit="return validatelogin()" method="post" >
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="panel-body">
                                    <div class="form" >

                                        <div class="form-group ">
                                            <label class="control-label col-lg-2">Slot One Start Time</label>
                                            <div class="col-lg-6">
                                                <input type="text" class="form-control" placeholder="Slot One Start Time" name="slot_one_starttime" value="<?php echo $list['slot_one_starttime'];?>" id="timepicker" >
                                                <input type="hidden" name="extra_charges_type" value="<?php echo $list['extra_charges_type'];?>">
                                                <input type="hidden" name="extra_charges_id" value="<?php echo $list['extra_charges_id'];?>">
                                            </div>
                                        </div>

                                        <div class="form-group ">
                                            <label class="control-label col-lg-2">Slot One End Time</label>
                                            <div class="col-lg-6">
                                                <input type="text" class="form-control" placeholder="Slot One End Time" name="slot_one_endtime" value="<?php echo $list['slot_one_endtime'];?>" id="timepicker1" >
                                            </div>
                                        </div>
                                        <?php if($list['extra_charges_type'] == 1){ ?>
                                            <div class="form-group ">
                                                <label class="control-label col-lg-2">Slot Two Start Time</label>
                                                <div class="col-lg-6">
                                                    <input type="text" class="form-control" placeholder="Slot Two Start Time" name="slot_two_starttime" value="<?php echo $list['slot_two_starttime'];?>" id="timepicker1">
                                                </div>
                                            </div>

                                            <div class="form-group ">
                                                <label class="control-label col-lg-2">Slot Two End Time</label>
                                                <div class="col-lg-6">
                                                    <input type="text" class="form-control" placeholder="Slot Two End Time" name="slot_two_endtime" value="<?php echo $list['slot_two_endtime'];?>" id="timepicker1">
                                                </div>
                                            </div>
                                        <?php } ?>


                                        <div class="form-group ">
                                            <label class="control-label col-lg-2">Charges Type*</label>
                                            <div class="col-lg-6">
                                                <select class="form-control" name="payment_type" id="payment_type">
                                                    <option value="">Select Payment</option>
                                                    <option value="1" <?php if ($list['payment_type'] == 1){ ?> selected <?php } ?>>Nominal</option>
                                                    <option value="2" <?php if ($list['payment_type'] == 2){ ?> selected <?php } ?>>Multiplier</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group ">
                                            <label class="control-label col-lg-2">Charges</label>
                                            <div class="col-lg-6">
                                                <input type="text" class="form-control" placeholder="Enter Charges" value="<?php echo $list['slot_price'];?>" name="slot_price"  id="slot_price">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-lg-offset-2 col-lg-10">
                                                <input type="submit" class=" btn btn-info col-md-4 col-sm-6 col-xs-12 black-background white" id="save" name="Update" value="Save Changes" >
                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </div>
                        </div>
                    </form>

                </div>
                <div class="clear"></div>
            </div>
        </div>
    </div>

</div>
<script>
    jQuery(document).ready(function() {
        var timeAnswers =$('#timepicker, #timepicker1');
        $(timeAnswers).each(function(){
            $(this).timepicki({
                overflow_minutes:true,
                increase_direction:'up',
                disable_keyboard_mobile: true
            });
        });
    });
</script>
<!-- Page Content Ends -->
<!-- ================== -->

</section>
<!-- Main Content Ends -->

</body>
</html>
