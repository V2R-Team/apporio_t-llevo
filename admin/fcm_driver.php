<?php
function AndroidPushNotification($did, $msg) {
	
	$url = 'https://fcm.googleapis.com/fcm/send';
	
	$fields = array (
			'to' 		        => $did,
			'data' => array ("message" => $msg) 
	);
	$headers = array (
			'Authorization: key=AAAAPyyd3_w:APA91bEloweypD5WgsPfisku-DEjiVqZOK6OEYs1JbVpeQDk6dpuGZKO593Hkwx5HZcodfMiAdjML-MJ7rFbX7_YrKA3SVRe91fymVMJM0bDVKZIf-3s1VL8f47ewfqkXkqB8Q-wLEyH',
			'Content-Type: application/json' );
	// Open connection
	$ch = curl_init ();
	// Set the url, number of POST vars, POST data
	curl_setopt ( $ch, CURLOPT_URL, $url );
	curl_setopt ( $ch, CURLOPT_POST, true );
	curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
	curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 ); 
	curl_setopt ( $ch, CURLOPT_POSTFIELDS, json_encode ( $fields ) );
	// Execute post
	$result = curl_exec ( $ch );
	// Close connection
	curl_close ( $ch );
        return $result;
}
?>