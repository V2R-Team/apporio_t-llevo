<?php
function AndroidPushNotificationCustomer($did, $msg,$ride_id,$ride_status)
{
    // Set POST variables
    $url = 'https://fcm.googleapis.com/fcm/send';

    $app_id="1";

    $fields = array ('to' => $did,'priority' => 'high','data' => array('message' => $msg,'ride_id'=>$ride_id,'ride_status'=> $ride_status,'app_id'=>$app_id) );

    $headers = array (
        'Authorization: key=AAAAKS3E_F8:APA91bH6_8gwzJ2vxAj0lH9oQ9JC-gXKjW-_LQBeoMSFCsMiietT6kEp4w3Z4iX9Yg9RLLqkgil7fo3taGUCR3HDToPlpDCPJzCpfndx0pv4xHZEaiD45dLh0xxGppWbvQNo6MwfANNM',
        'Content-Type: application/json' );

    // Open connection
    $ch = curl_init ();
    // Set the url, number of POST vars, POST data
    curl_setopt ( $ch, CURLOPT_URL, $url );
    curl_setopt ( $ch, CURLOPT_POST, true );
    curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
    curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
    curl_setopt ( $ch, CURLOPT_POSTFIELDS,  json_encode ( $fields ) );
    // Execute post
    $result = curl_exec ( $ch );
    // Close connection
    curl_close ( $ch );
    return $result;
}

function AndroidPushNotificationDriver($did, $msg,$ride_id,$ride_status) {
    // Set POST variables
    $url = 'https://fcm.googleapis.com/fcm/send';

    $app_id="2";



    $fields = array ('to' => $did,'priority' => 'high','data' => array('message' => $msg,'ride_id'=>$ride_id,'ride_status'=> $ride_status,'app_id'=>$app_id) );

    $headers = array (
        'Authorization: key=AAAAKS3E_F8:APA91bH6_8gwzJ2vxAj0lH9oQ9JC-gXKjW-_LQBeoMSFCsMiietT6kEp4w3Z4iX9Yg9RLLqkgil7fo3taGUCR3HDToPlpDCPJzCpfndx0pv4xHZEaiD45dLh0xxGppWbvQNo6MwfANNM',
        'Content-Type: application/json' );

    // Open connection
    $ch = curl_init ();
    // Set the url, number of POST vars, POST data
    curl_setopt ( $ch, CURLOPT_URL, $url );
    curl_setopt ( $ch, CURLOPT_POST, true );
    curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
    curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
    curl_setopt ( $ch, CURLOPT_POSTFIELDS, json_encode ( $fields ) );
    // Execute post
    $result = curl_exec ( $ch );
    // Close connection
    curl_close ( $ch );

    return $result;

}
?>