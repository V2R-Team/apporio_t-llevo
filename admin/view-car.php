<?php
include_once '../apporioconfig/start_up.php';
if(!isset($_SESSION['ADMIN']['ID']))
{
    $db->redirect("index.php");
}
include('common.php');


    $query="select * from car_type";
	$result = $db->query($query);
	$list=$result->rows;       
        
   if(isset($_GET['status']) && isset($_GET['id'])) 
    {
     $query1="UPDATE car_type  SET car_admin_status='".$_GET['status']."' WHERE car_type_id='".$_GET['id']."'";
     $db->query($query1);
     $db->redirect("home.php?pages=view-car-type");
    }
    

	if(isset($_POST['savechanges'])) 
     {
         if($_FILES['car_type_image']['name'] == ""){
           $query2="UPDATE car_type  SET car_type_name='".$_POST['car_type_name']."',car_type_name_french='".$_POST['car_type_french']."' where car_type_id='".$_POST['savechanges']."'";
           $db->query($query2); 
           $db->redirect("home.php?pages=view-car-type");
         }else{
          $img_name = $_FILES['car_type_image']['name'];
          $filedir  = "../uploads/car/";
          if(!is_dir($filedir)) mkdir($filedir, 0755, true);
          $fileext = strtolower(substr($_FILES['car_type_image']['name'],-4));
          if($fileext==".jpg" || $fileext==".gif" || $fileext==".png" || $fileext=="jpeg") 
          {
          if($fileext=="jpeg") 
            {
           $fileext=".jpg";
           }
           $pfilename = "editcar_".$_POST['savechanges'].$fileext;
           $filepath1 = "uploads/car/".$pfilename;
           $filepath = $filedir.$pfilename;
           copy($_FILES['car_type_image']['tmp_name'], $filepath);
      
       $query2="UPDATE car_type  SET car_type_name='".$_POST['car_type_name']."',car_type_name_french='".$_POST['car_type_french']."',car_type_image='$filepath1' where car_type_id='".$_POST['savechanges']."'";
            $db->query($query2); 
            $db->redirect("home.php?pages=view-car-type");
        }
      }
     }
    
?>

<form method="post" name="frm">
<div class="wraper container-fluid">
  <div class="page-title">
    <h3 class="title">View Vehicle Type</h3>
      <span>
            <a href="home.php?pages=add-car-type" class="btn btn-default btn-lg" id="add-button" title="Add A Car Type" role="button">Add Vehicle Type</a>
      </span>
  </div>
  
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        
        <div class="panel-body">
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 mobtbl">
              <table id="datatable" class="table table-striped table-bordered table-responsive">
                <thead>
                  <tr>
                    <th width="5%">S.No</th>
                    <th width="50%">Vehicle Name</th>
                    <th width="50%">Vehicle Name French </th>
                    <th>Vehicle Image</th>
                    <th width="12%">Status</th>
                    <th width="4%">Edit</th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach($list as $cartype){?>
                  <tr>

                    <td><?php echo $cartype['car_type_id'];?></td>
                    
                    <td>
                    <?php
            	      $car_type_name  = $cartype['car_type_name'];
            	       echo $car_type_name;
            	      ?>
            	      </td>
            	       <td>
                    <?php
            	      $car_type_name_french=$cartype['car_type_name_french'];
            	      if($car_type_name_french=="")
            	      {
            	      echo "---------";
            	      }
            	      else
            	      {
            	       echo $car_type_name_french;
            	      }
            	      ?>
            	      </td>
            	      
            	      
            	     <td><img src="../<?php echo $cartype['car_type_image'];?>"  width="80px" height="80px"></td>
            	      
            	      
            	    <?php
                                if($cartype['car_admin_status']==1) {
                                ?>
                                <td class="text-center">
                                    <a href="home.php?pages=view-car-type&status=2&id=<?php echo $cartype['car_type_id']?>" class="" title="Active">
                                    <button type="button" class="btn btn-success br2 btn-xs fs12 activebtn" > Active
                                    </button></a>
                                </td>
                                <?php
                                } else {
                                ?>
                                <td class="text-center">
                                <a href="home.php?pages=view-car-type&status=1&id=<?php echo $cartype['car_type_id']?>" class="" title="Deactive">
                                    <button type="button" class="btn btn-danger  br2 btn-xs fs12 dropdown-toggle" > Deactive
                                    </button></a>
                                </td>
                                <?php } ?>
   <td><button type="button" class="btn btn-info glyphicon glyphicon-pencil" data-toggle="modal" data-target="#<?php echo $cartype['car_type_id']?>"  ></button></td>                
                  </tr>
                  <?php }?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- End row --> 
  
</div> 
</form>
<!-- Page Content Ends --> 
<!-- ================== -->
<?php foreach($list as $cartype){?>
<div class="modal fade" id="<?php echo $cartype['car_type_id']?>" role="dialog">
  <div class="modal-dialog"> 
    
    <!-- Modal content starts-->
    
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title fdetailsheading">Edit Vehicle Brand Details</h4>
      </div>
      <form  method="post" enctype="multipart/form-data" onSubmit="return validatelogin()">
        <div class="modal-body">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label for="field-3" class="control-label">Vehicle Name</label>
                <input type="text" class="form-control"  placeholder="Car Name" name="car_type_name" value="<?php echo $cartype['car_type_name'];?>" id="car_type_name" required>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label for="field-3" class="control-label">Vehicle Name In French</label>
                <input type="text" class="form-control"  placeholder="Car Name In french" name="car_type_french" value="<?php echo $cartype['car_type_name_french'];?>" id="car_type_french" required>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label for="field-3" class="control-label">Vehicle Image</label>
                <input type="file" name="car_type_image" id="car_type_image" />
              </div>
            </div>
          </div>
          
        </div>
        
        
        <div class="modal-footer">
          <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
          <button type="submit" name="savechanges" value="<?php echo $cartype['car_type_id']?>" class="btn btn-info">Save Changes</button>
        </div>
      </form>
    </div>
  </div>
</div>
<?php }?>
</section>
</body></html>