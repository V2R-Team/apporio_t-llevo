<?php
include_once '../apporioconfig/start_up.php';
if(!isset($_SESSION['Company']['ID']))
{
    $db->redirect("index.php");
}
$company_id = $_SESSION['Company']['ID'];
$query="select * from company WHERE company_id='$company_id'";
$result = $db->query($query);
$list=$result->row;
$pickup_location = $list['company_address'];

include('common.php');
$address = str_replace(" ", "+", $pickup_location);
$url = "http://maps.google.com/maps/api/geocode/json?address=$address&sensor=false";
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
$response = curl_exec($ch);
curl_close($ch);
$response_a = json_decode($response);
$lat = $response_a->results[0]->geometry->location->lat;

$long = $response_a->results[0]->geometry->location->lng;
?>

<div class="wraper container-fluid">
    <div class="page-title">
        <h3 class="title">Driver Maps</h3>
        <nav id="sidebar-wrapper" class="">
            <div id="driver_details">
                <ul class="sidebar-nav ">
                    <a id="menu-close" href="#" class="btn btn-light btn-lg pull-right toggle"><i style="color:#ffffff;" class="fa fa-times"></i></a>
                    <li style="clear:both;"></li>

                </ul>
            </div>
        </nav>

    </div>
    <div class="row">
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCOfKh2baXWXfu3rIQDnTnIWxviWpjSyNI"></script>
        <div id="map" style="width:100%;height:550px;  "></div>



        <script type="text/javascript">

            var locations = [
                <?php
                $query="select * from driver where online_offline=1 and busy != 1 AND driver.company_id='$company_id' ";
                $result = $db->query($query);
                $list=$result->rows;
                foreach($list as $driver) {
                $driver_id = $driver['driver_id'];
                $driver_name = $driver['driver_name'];
                $current_lat  = $driver['current_lat'];
                $current_long = $driver['current_long'];
                ?>
                ['<div style=" text-transform: capitalize; cursor:pointer;" class="map_details_click" onclick="showmapdetails(<?php echo $driver_id ?>)" ><?php echo $driver_name?></div>', <?php echo $current_lat ?>,<?php echo $current_long ?>,'<?php echo ICON_URL ?>'],
                <?php };?>
            ];

            var map = new google.maps.Map(document.getElementById('map'),{
                zoom: 6,
                center: new google.maps.LatLng(<?php echo $lat ?>, <?php echo $long ?>),
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });
            var infowindow = new google.maps.InfoWindow(), marker, i;

            for (i = 0; i < locations.length; i++) {

                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                    map: map,
                    icon: locations[i][3]
                });
                google.maps.event.addListener(marker, 'click', (function(marker, i) {
                    return function() {
                        infowindow.setContent(locations[i][0]);
                        infowindow.open(map, marker);
                    }
                })(marker, i));
            }

        </script>


        <script>

            function showmapdetails(driver_id) {
                $('#sidebar-wrapper').addClass("active");
                $.ajax({
                    url: "ajax.php?action=driver_details1&driver_id="+driver_id,
                    success: function(res) {
                        $('#driver_details').html(res);
                    }
                });
            }

            function showmapdetails1(driver_id) {
                $('#sidebar-wrapper').addClass("active");
                $.ajax({
                    url: "ajax.php?action=driver_details1_ride&driver_id="+driver_id,
                    success: function(res) {
                        $('#driver_details').html(res);
                    }
                });
            }


        </script>
    </div>
</div>
</section>
<!-- Main Content Ends -->

</body></html>