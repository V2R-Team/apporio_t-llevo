Rental - Car Type Api: November 17, 2017, 1:12 am
Response: Array
(
    [0] => Array
        (
            [car_type_id] => 2
            [car_type_name] => T-LLEVO ESTÃNDAR
            [car_type_image] => uploads/car/editcar_2.png
            [city_id] => 123
            [base_fare] => 3 Per 5 Km
            [ride_mode] => 1
        )

)

city_name: Gurugram
latitude: 28.414040019707542
longitude: 77.04403325915337
-------------------------
Rental - Car Type Api: November 17, 2017, 8:36 am
Response: Array
(
    [0] => Array
        (
            [car_type_id] => 2
            [car_type_name] => T-LLEVO ESTÃNDAR
            [car_type_image] => uploads/car/editcar_2.png
            [city_id] => 120
            [base_fare] => 35 Per 2 Km
            [ride_mode] => 1
        )

    [1] => Array
        (
            [car_type_id] => 3
            [car_type_name] => T-LLEVO CAMIONETA
            [car_type_image] => uploads/car/editcar_3.png
            [city_id] => 120
            [base_fare] => 60 Per 2 Km
            [ride_mode] => 1
        )

    [2] => Array
        (
            [car_type_id] => 4
            [car_type_name] => T-LLEVO DE LUJO
            [car_type_image] => uploads/car/editcar_4.png
            [city_id] => 120
            [base_fare] => 100 Per 2 Km
            [ride_mode] => 1
        )

)

city_name: Cuautitlán Izcalli
latitude: 19.662734646319368
longitude: -99.20861128717662
-------------------------
Rental - Car Type Api: November 17, 2017, 8:39 am
Response: Array
(
    [0] => Array
        (
            [car_type_id] => 2
            [car_type_name] => T-LLEVO ESTÃNDAR
            [car_type_image] => uploads/car/editcar_2.png
            [city_id] => 120
            [base_fare] => 35 Per 2 Km
            [ride_mode] => 1
        )

    [1] => Array
        (
            [car_type_id] => 3
            [car_type_name] => T-LLEVO CAMIONETA
            [car_type_image] => uploads/car/editcar_3.png
            [city_id] => 120
            [base_fare] => 60 Per 2 Km
            [ride_mode] => 1
        )

    [2] => Array
        (
            [car_type_id] => 4
            [car_type_name] => T-LLEVO DE LUJO
            [car_type_image] => uploads/car/editcar_4.png
            [city_id] => 120
            [base_fare] => 100 Per 2 Km
            [ride_mode] => 1
        )

)

city_name: Cuautitlán Izcalli
latitude: 19.66380337540181
longitude: -99.2063471674919
-------------------------
Rental - Car Type Api: November 17, 2017, 8:40 am
Response: Array
(
    [0] => Array
        (
            [car_type_id] => 2
            [car_type_name] => T-LLEVO ESTÃNDAR
            [car_type_image] => uploads/car/editcar_2.png
            [city_id] => 120
            [base_fare] => 35 Per 2 Km
            [ride_mode] => 1
        )

    [1] => Array
        (
            [car_type_id] => 3
            [car_type_name] => T-LLEVO CAMIONETA
            [car_type_image] => uploads/car/editcar_3.png
            [city_id] => 120
            [base_fare] => 60 Per 2 Km
            [ride_mode] => 1
        )

    [2] => Array
        (
            [car_type_id] => 4
            [car_type_name] => T-LLEVO DE LUJO
            [car_type_image] => uploads/car/editcar_4.png
            [city_id] => 120
            [base_fare] => 100 Per 2 Km
            [ride_mode] => 1
        )

)

city_name: Cuautitlán Izcalli
latitude: 19.662646558730117
longitude: -99.20858614146711
-------------------------
Rental - Car Type Api: November 17, 2017, 9:43 am
Response: Array
(
    [0] => Array
        (
            [car_type_id] => 2
            [car_type_name] => T-LLEVO ESTÃNDAR
            [car_type_image] => uploads/car/editcar_2.png
            [city_id] => 120
            [base_fare] => 35 Per 2 Km
            [ride_mode] => 1
        )

    [1] => Array
        (
            [car_type_id] => 3
            [car_type_name] => T-LLEVO CAMIONETA
            [car_type_image] => uploads/car/editcar_3.png
            [city_id] => 120
            [base_fare] => 60 Per 2 Km
            [ride_mode] => 1
        )

    [2] => Array
        (
            [car_type_id] => 4
            [car_type_name] => T-LLEVO DE LUJO
            [car_type_image] => uploads/car/editcar_4.png
            [city_id] => 120
            [base_fare] => 100 Per 2 Km
            [ride_mode] => 1
        )

)

city_name: Huilango
latitude: 19.6732833377036
longitude: -99.23029359430075
-------------------------
Rental - Car Type Api: November 17, 2017, 12:04 pm
Response: Array
(
    [0] => Array
        (
            [car_type_id] => 2
            [car_type_name] => T-LLEVO ESTÃNDAR
            [car_type_image] => uploads/car/editcar_2.png
            [city_id] => 120
            [base_fare] => 35 Per 2 Km
            [ride_mode] => 1
        )

    [1] => Array
        (
            [car_type_id] => 3
            [car_type_name] => T-LLEVO CAMIONETA
            [car_type_image] => uploads/car/editcar_3.png
            [city_id] => 120
            [base_fare] => 60 Per 2 Km
            [ride_mode] => 1
        )

    [2] => Array
        (
            [car_type_id] => 4
            [car_type_name] => T-LLEVO DE LUJO
            [car_type_image] => uploads/car/editcar_4.png
            [city_id] => 120
            [base_fare] => 60 Per 2 Km
            [ride_mode] => 1
        )

)

city_name: Huilango
latitude: 19.673277970714032
longitude: -99.23029560595751
-------------------------
Rental - Car Type Api: November 17, 2017, 12:19 pm
Response: Array
(
    [0] => Array
        (
            [car_type_id] => 2
            [car_type_name] => T-LLEVO ESTÃNDAR
            [car_type_image] => uploads/car/editcar_2.png
            [city_id] => 120
            [base_fare] => 35 Per 2 Km
            [ride_mode] => 1
        )

    [1] => Array
        (
            [car_type_id] => 3
            [car_type_name] => T-LLEVO CAMIONETA
            [car_type_image] => uploads/car/editcar_3.png
            [city_id] => 120
            [base_fare] => 60 Per 2 Km
            [ride_mode] => 1
        )

    [2] => Array
        (
            [car_type_id] => 4
            [car_type_name] => T-LLEVO DE LUJO
            [car_type_image] => uploads/car/editcar_4.png
            [city_id] => 120
            [base_fare] => 60 Per 2 Km
            [ride_mode] => 1
        )

)

city_name: Huilango
latitude: 19.67302824764726
longitude: -99.23007398843765
-------------------------
Rental - Car Type Api: November 17, 2017, 3:42 pm
Response: Array
(
    [0] => Array
        (
            [car_type_id] => 2
            [car_type_name] => T-LLEVO ESTÃNDAR
            [car_type_image] => uploads/car/editcar_2.png
            [city_id] => 120
            [base_fare] => 35 Per 2 Km
            [ride_mode] => 1
        )

    [1] => Array
        (
            [car_type_id] => 3
            [car_type_name] => T-LLEVO CAMIONETA
            [car_type_image] => uploads/car/editcar_3.png
            [city_id] => 120
            [base_fare] => 60 Per 2 Km
            [ride_mode] => 1
        )

    [2] => Array
        (
            [car_type_id] => 4
            [car_type_name] => T-LLEVO DE LUJO
            [car_type_image] => uploads/car/editcar_4.png
            [city_id] => 120
            [base_fare] => 60 Per 2 Km
            [ride_mode] => 1
        )

)

city_name: Cuautitlán
latitude: 19.607132607515204
longitude: -99.18892957270147
-------------------------
Rental - Car Type Api: November 17, 2017, 3:42 pm
Response: Array
(
    [0] => Array
        (
            [car_type_id] => 2
            [car_type_name] => T-LLEVO ESTÃNDAR
            [car_type_image] => uploads/car/editcar_2.png
            [city_id] => 120
            [base_fare] => 35 Per 2 Km
            [ride_mode] => 1
        )

    [1] => Array
        (
            [car_type_id] => 3
            [car_type_name] => T-LLEVO CAMIONETA
            [car_type_image] => uploads/car/editcar_3.png
            [city_id] => 120
            [base_fare] => 60 Per 2 Km
            [ride_mode] => 1
        )

    [2] => Array
        (
            [car_type_id] => 4
            [car_type_name] => T-LLEVO DE LUJO
            [car_type_image] => uploads/car/editcar_4.png
            [city_id] => 120
            [base_fare] => 60 Per 2 Km
            [ride_mode] => 1
        )

)

city_name: Cuautitlán Izcalli
latitude: 19.606603898235807
longitude: -99.18866101652382
-------------------------
Rental - Car Type Api: November 17, 2017, 3:43 pm
Response: Array
(
    [0] => Array
        (
            [car_type_id] => 2
            [car_type_name] => T-LLEVO ESTÃNDAR
            [car_type_image] => uploads/car/editcar_2.png
            [city_id] => 120
            [base_fare] => 35 Per 2 Km
            [ride_mode] => 1
        )

    [1] => Array
        (
            [car_type_id] => 3
            [car_type_name] => T-LLEVO CAMIONETA
            [car_type_image] => uploads/car/editcar_3.png
            [city_id] => 120
            [base_fare] => 60 Per 2 Km
            [ride_mode] => 1
        )

    [2] => Array
        (
            [car_type_id] => 4
            [car_type_name] => T-LLEVO DE LUJO
            [car_type_image] => uploads/car/editcar_4.png
            [city_id] => 120
            [base_fare] => 60 Per 2 Km
            [ride_mode] => 1
        )

)

city_name: Cuautitlán
latitude: 19.60805042248355
longitude: -99.19026464223862
-------------------------
Rental - Car Type Api: November 17, 2017, 4:22 pm
Response: Array
(
    [0] => Array
        (
            [car_type_id] => 2
            [car_type_name] => T-LLEVO ESTÃNDAR
            [car_type_image] => uploads/car/editcar_2.png
            [city_id] => 120
            [base_fare] => 35 Per 2 Km
            [ride_mode] => 1
        )

    [1] => Array
        (
            [car_type_id] => 3
            [car_type_name] => T-LLEVO CAMIONETA
            [car_type_image] => uploads/car/editcar_3.png
            [city_id] => 120
            [base_fare] => 60 Per 2 Km
            [ride_mode] => 1
        )

    [2] => Array
        (
            [car_type_id] => 4
            [car_type_name] => T-LLEVO DE LUJO
            [car_type_image] => uploads/car/editcar_4.png
            [city_id] => 120
            [base_fare] => 60 Per 2 Km
            [ride_mode] => 1
        )

)

city_name: Cuautitlán Izcalli
latitude: 19.69215003610217
longitude: -99.21780858188868
-------------------------
Rental - Car Type Api: November 17, 2017, 8:27 pm
Response: Array
(
    [0] => Array
        (
            [car_type_id] => 2
            [car_type_name] => T-LLEVO ESTÃNDAR
            [car_type_image] => uploads/car/editcar_2.png
            [city_id] => 120
            [base_fare] => 35 Per 2 Km
            [ride_mode] => 1
        )

    [1] => Array
        (
            [car_type_id] => 3
            [car_type_name] => T-LLEVO CAMIONETA
            [car_type_image] => uploads/car/editcar_3.png
            [city_id] => 120
            [base_fare] => 60 Per 2 Km
            [ride_mode] => 1
        )

    [2] => Array
        (
            [car_type_id] => 4
            [car_type_name] => T-LLEVO DE LUJO
            [car_type_image] => uploads/car/editcar_4.png
            [city_id] => 120
            [base_fare] => 60 Per 2 Km
            [ride_mode] => 1
        )

)

city_name: Cuautitlán Izcalli
latitude: 19.691291416623518
longitude: -99.22620121389627
-------------------------
